package com.company.task1;

import java.util.List;

public class FirstTask {

    public static List<Integer> square56(List<Integer> numbers) {
        return numbers.stream()
                .map(n -> (n * n + 10))
                .filter(n -> n % 10 != 5 && n % 10 != 6)
                .toList();
    }

    public static void main(String[] args) {
    }
}
