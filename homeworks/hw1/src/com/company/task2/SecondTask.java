package com.company.task2;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SecondTask {

    public static Map<Integer, Long> getMapDuplicateCount(List<Integer> numbers) {
        return numbers.stream()
                .collect(Collectors.groupingBy(x -> x, Collectors.counting()))
                .entrySet().stream().filter(x -> x.getValue() > 1)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    public static void main(String[] args) {
    }
}
