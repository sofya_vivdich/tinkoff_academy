package com.company.task3;

public class ThirdTask {
    public static void start() {
        TaskRunner taskRunner = new TaskRunner(1);

        Thread thread1 = new Thread(() -> taskRunner.run(1));
        Thread thread2 = new Thread(() -> taskRunner.run(2));
        Thread thread3 = new Thread(() -> taskRunner.run(3));

        thread1.start();
        thread2.start();
        thread3.start();
    }

    public static void main(String[] args) {
        start();
    }
}
