package com.company.task3;

public class TaskRunner {
    private int activeThreadNumber;
    private Object lock = new Object();

    public TaskRunner(int activeThreadNumber) {
        this.activeThreadNumber = activeThreadNumber;
    }

    public void run(int threadNum) {
        for (int i = 0; i < 6; i++) {
            synchronized (lock) {
                while (activeThreadNumber != threadNum) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        System.out.println("Thread Interrupted");
                    }
                }
                System.out.print(threadNum);

                activeThreadNumber = threadNum % 3 + 1;
                lock.notifyAll();
            }
        }
    }
}
