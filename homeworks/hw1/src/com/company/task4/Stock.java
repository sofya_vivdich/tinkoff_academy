package com.company.task4;

import java.util.concurrent.ArrayBlockingQueue;

public class Stock {
    private volatile ArrayBlockingQueue<Product> queue;

    public Stock(int capacity) {
        this.queue = new ArrayBlockingQueue<>(capacity, true);
    }

    public ArrayBlockingQueue<Product> getQueue() {
        return queue;
    }

    public void add(Product product) {
        queue.add(product);
    }

    public void buy() {
        queue.poll();
    }

    public void onNotifyAll() {
        synchronized (queue) {
            queue.notifyAll();
        }
    }

    public void onWait() throws InterruptedException {
        synchronized (queue) {
            queue.wait();
        }
    }
}
