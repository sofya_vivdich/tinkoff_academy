package com.company.task4;

public class Producer implements Runnable{
    private Stock stock;

    public Producer(Stock stock) {
        this.stock = stock;
    }

    @Override
    public void run() {
        try {
            produceProduct();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void produceProduct() throws InterruptedException {
        int i = 0;
        while (true) {
            while (stock.getQueue().remainingCapacity() == 0) {
                try {
                    stock.onWait();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    System.out.println("Producer thread Interrupted");
                }
            }
            stock.add(new Product(i++));
            System.out.println("Товар добавлен. Товаров на складе: " + stock.getQueue().size());
            Thread.sleep(3000);
            stock.onNotifyAll();
        }
    }
}
