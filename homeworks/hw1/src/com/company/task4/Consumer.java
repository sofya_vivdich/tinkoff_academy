package com.company.task4;

public class Consumer implements Runnable {
    private Stock stock;

    public Consumer(Stock stock) {
        this.stock = stock;
    }

    @Override
    public void run() {
        try {
            buyProduct();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void buyProduct() throws InterruptedException {
        while (true) {
            while (stock.getQueue().isEmpty()) {
                try {
                    stock.onWait();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    System.out.println("Thread Interrupted");
                }
            }
            stock.buy();
            System.out.println("Товар куплен. Товаров на складе: " + stock.getQueue().size());
            stock.onNotifyAll();
            Thread.sleep(5000);
        }
    }
}
