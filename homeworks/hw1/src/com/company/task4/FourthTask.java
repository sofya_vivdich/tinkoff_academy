package com.company.task4;

public class FourthTask {
    public static void main(String[] args) {
        Stock stock = new Stock(100);
        Thread producer = new Thread(new Producer(stock));
        Thread consumer1 = new Thread(new Consumer(stock));
        Thread consumer2 = new Thread(new Consumer(stock));

        producer.start();
        consumer1.start();
        consumer2.start();
    }
}
